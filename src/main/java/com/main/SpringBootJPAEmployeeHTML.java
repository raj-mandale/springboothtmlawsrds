package com.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJPAEmployeeHTML {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJPAEmployeeHTML.class, args);
    }

}
